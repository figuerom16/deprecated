#!/bin/bash
echo "This is a template of how to fully press a Node server in LXC Debian"
echo "Your project name will be the folder names (under /opt)."
echo "You can always change both later"
echo "**********************************"
echo
echo "WHAT IS THE PROJECT NAME?: "
echo "(Forced to lower and spaces are removed)"
echo "(Use underscores as this will be used for usernames)"
read project
project=$(echo ${project,,} | tr -d " \t\n\r")
echo "Project Name: $project"
cd /opt
mkdir $project && cd $project
echo "alias fixperm='chown -R www-data:www-data /opt/*'" >> ~/.bashrc
chown -R www-data:www-data /opt/*
cat << EOT > /etc/systemd/system/node.service
[Unit]
Description=node daemon
After=network.target

[Service]
User=www-data
Group=www-data
WorkingDirectory=/opt/$project
ExecStart=/usr/bin/node build/index.js

[Install]
WantedBy=multi-user.target
EOT
systemctl daemon-reload
systemctl start node.service
systemctl enable node.service
cat << EOT > /etc/nginx/sites-available/$project
server {
	listen 80;
	charset utf-8;
	root /opt/$project/;
	location = /favicon.png {access_log off; log_not_found off;}
	location /static/ {access_log off;}
	location / {
		include proxy_params;
		proxy_pass http://unix:/tmp/node.sock;
	}
}
EOT
ln -s /etc/nginx/sites-available/$project /etc/nginx/sites-enabled/$project
rm /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
systemctl restart nginx
echo "cd /opt/$project" >> ~/.bashrc
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
echo
echo "Aliases created in bashrc. Reboot or run below to use."
echo "alias fixperm='chown -R www-data:www-data /opt/*'"