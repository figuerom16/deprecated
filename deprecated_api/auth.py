from datetime import datetime, timedelta
from secrets import compare_digest, token_urlsafe
from fastapi import Request, HTTPException
import env

def iso_date(days:int=0): return (datetime.utcnow()+timedelta(days)).isoformat().split('.')[0]+'Z'
def create_token(days:int=30): return iso_date(days)+token_urlsafe(48)
def get_cookie(auth_str:str): return {'name':env.cookie_name, 'value':auth_str, 'path':'/'}
def unparse_auth(dtoken:str, email:str): return dtoken.replace(':','%3A')+email.replace('@','%40')
def parse_auth(auth_str:str):
	auth_str = auth_str.replace('%3A',':').replace('%40','@')
	if len(auth_str) < 90:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	maxage, token, email = auth_str[:20], auth_str[20:84], auth_str[84:]
	if maxage[19] != 'Z' or '@' not in email or maxage < iso_date():
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	return maxage+token, email

async def get_user(req:Request, auth_str:str=''):
	if 'authorization' in req.headers:
		bearer_str = req.headers.get('authorization')
		if not bearer_str: 
			raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
		dtoken, email = parse_auth(bearer_str)
		db_id = await req.app.db.hget('ix:email', email)
		bearer, locked = await req.app.db.hmget(db_id, 'bearer', 'locked')
		if locked is not None:
			raise HTTPException(status_code=401, detail=['LOCKED: ' + locked])
		if bearer[:20] < iso_date() or not compare_digest(bearer, dtoken):
			raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	elif auth_str or env.cookie_name in req.cookies:
		if not auth_str: auth_str = req.cookies[env.cookie_name]
		dtoken, email = parse_auth(auth_str)
		db_id = await req.app.db.hget('ix:email', email)
		up_dtoken = await req.app.db.hget('ix:up_email', email)
		if db_id is None:
			if up_dtoken is not None and up_dtoken[:20] > iso_date() and compare_digest(up_dtoken, dtoken):
				return {'email':email, 'dtoken':up_dtoken}
			else:
				raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
		db_dtoken, locked = await req.app.db.hmget(db_id, 'token', 'locked')
		if locked is not None:
			raise HTTPException(status_code=401, detail=['LOCKED: ' + locked])
		if up_dtoken is not None and up_dtoken[:20] > iso_date() and compare_digest(up_dtoken, dtoken):
			async with req.app.db.pipeline() as pipe:
				pipe.hdel('ix:up_email', email)
				pipe.hset(db_id, 'token', dtoken)
				await pipe.execute()
		elif db_dtoken[:20] < iso_date() or not compare_digest(db_dtoken, dtoken):
			await req.app.db.hincrby(db_id, 'login_trys')
			raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	else:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	await req.app.db.hset(db_id, 'date_interact', iso_date())
	key_list = ['id', 'email', 'name', 'role']
	user = await req.app.db.hmget(db_id, key_list)
	return {key:user[i] for i, key in enumerate(key_list)} | {'dtoken':dtoken}