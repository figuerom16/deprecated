from fastapi import APIRouter, HTTPException, Request
from models import AdminView
import auth

router = APIRouter()

@router.get('/admin',response_model=list[AdminView])
async def admin_dash(req:Request):
	db_user = await auth.get_user(req)
	if 'id' not in db_user:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	if db_user['role'] is None or db_user['role'] != 'ADMIN':
		raise HTTPException(status_code=401, detail=['ERROR: UNATHORIZED USER'])
	id_list = await req.app.db.keys('id:*')
	key_list = ['id', 'locked', 'email', 'name', 'role', 'login_trys', 'date_interact', 'date_registered']
	async with req.app.db.pipeline() as pipe:
		for id in id_list: pipe.hmget(id, key_list)
		user_list = await pipe.execute()
	for i, user in enumerate(user_list): user_list[i] = {key:user[j] for j, key in enumerate(key_list)}
	return user_list

@router.get('/admin/{email}',response_model=AdminView)
async def admin_get_user(req:Request, email:str):
	db_user = await auth.get_user(req)
	if 'id' not in db_user:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	if db_user['role'] is None or db_user['role'] != 'ADMIN':
		raise HTTPException(status_code=401, detail=['ERROR: UNATHORIZED USER'])
	key_list = ['id', 'locked', 'email', 'name', 'role', 'login_trys', 'date_interact', 'date_registered']
	key = await req.app.db.hget('ix:email', email)
	user = await req.app.db.hmget(key, key_list)
	if user is None:
		raise HTTPException(status_code=404, detail=['ERROR: User Not Found'])
	return {key:user[i] for i, key in enumerate(key_list)}

@router.post('/admin/{email}',response_model=AdminView)
async def admin_update_user(req:Request, email:str, user:AdminView):
	db_user = await auth.get_user(req)
	if 'id' not in db_user:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	if db_user['role'] is None or db_user['role'] != 'ADMIN':
		raise HTTPException(status_code=401, detail=['ERROR: UNATHORIZED USER'])
	key = await req.app.db.hget('ix:email', email)
	async with req.app.db.pipeline() as pipe:
		if await req.app.db.hget(key, 'email') != user.email:
			if await req.app.db.hget('ix:email', user.email) is not None: 
				raise HTTPException(status_code=400, detail=['ERROR: Email Conflict'])
			pipe.hset(key, 'email', user.email)
		pipe.hset(key, 'name', user.name)
		if user.locked is not None: pipe.hset(key, 'locked', user.locked)
		else:
			pipe.hdel(key, 'locked')
			pipe.hset(key, 'login_trys', 0)
		if user.role is not None: pipe.hset(key, 'role', user.role)
		else: pipe.hdel(key, 'role')
		await pipe.execute()
	return {'locked':user.locked, 'name':user.name, 'email':user.email, 'role':user.role}