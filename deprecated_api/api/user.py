from typing import Union
from fastapi import APIRouter, HTTPException, Request, Response
from email.message import EmailMessage
from models import UserAccount
import auth, env, json, smtplib

router = APIRouter()

@router.post('/', response_model=None)
async def update(req:Request, user:UserAccount):
	db_user = await auth.get_user(req)
	if db_user['dtoken'][:20] < auth.iso_date(29):
		raise HTTPException(status_code=400, detail=['ERROR: Token must be less than 1 day old to UPDATE ACCOUNT. Please Click GET NEW TOKEN and Login.'])
	dtoken, email = db_user['dtoken'], db_user['email']
	if 'id' not in db_user:
		id = await req.app.db.hget('ix:count', 'id')
		key = 'id:' + id
		async with req.app.db.pipeline() as pipe:
			pipe.hset('ix:email', user.email, key)
			pipe.hset(key, mapping={
				'id':id,
				'email':email,
				'name':user.name,
				'token':dtoken,
				'login_trys':0,
				'date_registered':auth.iso_date()})
			pipe.hincrby('ix:count', 'id')
			pipe.hdel('ix:up_email', db_user['email'])
			pipe.hset(key, 'token', db_user['dtoken'])
			if user.bearer_update is not None:
				if user.bearer_update == 'create':
					pipe.hset(key, 'bearer', auth.create_token(365))
				elif user.bearer_update == 'delete':
					pipe.hdel(key, 'bearer')
			await pipe.execute()
	else:
		key = 'id:' + db_user['id']
		async with req.app.db.pipeline() as pipe:
			pipe.hset(key, 'name', user.name)
			if user.bearer_update is not None:
				if user.bearer_update == 'create':
					pipe.hset(key, 'bearer', auth.create_token(365))
				elif user.bearer_update == 'delete':
					pipe.hdel(key, 'bearer')
			if db_user['email'] != user.email:
				req_email = await req.app.db.hget('ix:email', user.email)
				if req_email is not None:
					raise HTTPException(status_code=400, detail=['ERROR: EMAIL CONFLICT'])
				up_dtoken = await req.app.db.hget('ix:up_email', user.email)
				if up_dtoken is not None and up_dtoken[:20] > auth.iso_date(23):
					raise HTTPException(status_code=400, detail=['ERROR: EMAIL SENT RECENTLY'])
				up_dtoken = auth.create_token()
				await pipe.hset('ix:up_email', user.email, up_dtoken)
				msg = EmailMessage()
				msg['From'] = env.email_handler
				msg['To'] = user.email
				msg['Subject'] = 'Mattascale NEW EMAIL CONFIRMATION'
				body = f'''\
<html>
	<head></head>
	<body>
		<h1>CONFIRM NEW EMAIL:</h1>
		<p>Clicking This Button Will CONFIRM NEW EMAIL ADDRESS. If this action was not you then this email can be ignored.</p>
		<a href="{env.front_server}/token/{auth.unparse_auth(dtoken, db_user['email'])}/{up_dtoken+user.email.replace('@','%40')}"
			style="background-color: #1c87c9; border: none; color: white; padding: 20px 34px; text-align: center; text-decoration: none; display: inline-block; font-size: 20px; margin: 4px 2px; cursor: pointer;">EMAIL UPDATE</a>
		</br>
		<p>MATTASCALE</p>
	</body>
</html>'''
				msg.set_content(body, subtype='html')
				with smtplib.SMTP(env.email_server, env.email_port) as server:
					server.starttls()
					server.login(env.email_handler, env.email_password)
					server.send_message(msg)
			await pipe.execute()

@router.get('/bearer', response_model=Union[dict,None])
async def bearer(req:Request):
	db_user = await auth.get_user(req)
	if 'id' not in db_user:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	key = 'id:' + db_user['id']
	bearer = await req.app.db.hget(key, 'bearer')
	if bearer is not None:
		bearer += db_user['email']
	return {'bearer':bearer}

@router.get('/get_user', response_model=dict)
async def get_user(req:Request):
	db_user = await auth.get_user(req)
	if 'id' not in db_user: return {'email':db_user['email']}
	return db_user

@router.get('/get_user_header', response_model=None)
async def get_user_header(req:Request, res:Response):
	db_user = await auth.get_user(req)
	if 'id' not in db_user:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	res.headers['user_id'] = db_user['id']
	db_user.pop('id')
	db_user.pop('dtoken')
	res.headers['user_data'] = json.dumps(db_user)