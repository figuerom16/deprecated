from fastapi import APIRouter, HTTPException, Request
from models import UserBase, UserCookie
from email.message import EmailMessage
import auth, env, smtplib

router = APIRouter()

@router.post('/token', response_model=None)
async def login(req:Request, user:UserBase):
	key = await req.app.db.hget('ix:email', user.email)
	up_dtoken = await req.app.db.hget('ix:up_email', user.email)
	if key is not None:
		locked = await req.app.db.hget(key, 'locked')
		if locked is not None:
			raise HTTPException(status_code=401, detail=['LOCKED: ' + locked])
	if 'cookies' in req.headers or 'authorization' in req.headers:
		db_user = await auth.get_user(req)
		if 'id' in db_user: user.email = db_user['email']
	elif up_dtoken and up_dtoken[:20] > auth.iso_date(23):
		raise HTTPException(status_code=401, detail=['ERROR: EMAIL RECENTLY SENT / CHECK EMAIL FOR TOKEN'])
	dtoken = auth.create_token()
	await req.app.db.hset('ix:up_email', user.email, dtoken)
	msg = EmailMessage()
	msg['From'] = env.email_handler
	msg['To'] = user.email
	msg['Subject'] = 'Mattascale Token'
	body = f'''\
<html>
	<head></head>
	<body>
		<h1>MATTASCALE</h1>
		<p>PERSONAL LOGIN PAGE</p>
		<a href="{env.front_server}/token/{auth.unparse_auth(dtoken,user.email)}"
			style="background-color: #1c87c9; border: none; color: white; padding: 20px 34px; text-align: center; text-decoration: none; display: inline-block; font-size: 20px; margin: 4px 2px; cursor: pointer;">LOGIN</a>
		</br>
		<p>MATTASCALE</p>
	</body>
</html>'''
	msg.set_content(body, subtype='html')
	with smtplib.SMTP(env.email_server, env.email_port) as server:
		server.starttls()
		server.login(env.email_handler, env.email_password)
		server.send_message(msg)

@router.post('/token/{auth_str}', response_model=UserCookie)
async def token(req:Request, auth_str:str):
	await auth.get_user(req, auth_str)
	return auth.get_cookie(auth_str)

@router.post('/token/{auth_str}/{new_auth_str}', response_model=UserCookie)
async def email_update(req:Request, auth_str:str, new_auth_str:str):
	db_user = await auth.get_user(req, auth_str)
	if 'id' not in db_user:
		raise HTTPException(status_code=401, detail=['ERROR: BAD TOKEN'])
	await auth.get_user(req, new_auth_str)
	email = auth_str[84:]
	new_dtoken, new_email = new_auth_str[:84], new_auth_str[84:]
	async with req.app.db.pipeline() as pipe:
		key = 'id:' + db_user['id']
		pipe.hdel('ix:up_email', email)
		pipe.hdel('ix:email', email)
		pipe.hset('ix:email', new_email, key)
		pipe.hset(key, 'email', new_email)
		pipe.hset(key, 'token', new_dtoken)
		await pipe.execute()
	msg = EmailMessage()
	msg['From'] = env.email_handler
	msg['To'] = new_email
	msg['Subject'] = 'Mattascale Token'
	body = f'''\
<html>
	<head></head>
	<body>
		<h1>MATTASCALE</h1>
		<p>PERSONAL LOGIN PAGE</p>
		<a href="{env.front_server}/token/{auth.unparse_auth(new_dtoken, new_email)}"
			style="background-color: #1c87c9; border: none; color: white; padding: 20px 34px; text-align: center; text-decoration: none; display: inline-block; font-size: 20px; margin: 4px 2px; cursor: pointer;">LOGIN</a>
		</br>
		<p>MATTASCALE</p>
	</body>
</html>'''
	msg.set_content(body, subtype='html')
	with smtplib.SMTP(env.email_server, env.email_port) as server:
		server.starttls()
		server.login(env.email_handler, env.email_password)
		server.send_message(msg)
	return auth.get_cookie(new_dtoken+new_email)