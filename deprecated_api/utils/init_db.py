#First Run DB init
import asyncio
from auth import iso_date
from redis import asyncio as aioredis
from env import admin_name, email_handler

async def main():
	db = await aioredis.from_url('unix:/var/run/redis/redis.sock', decode_responses=True)
	if(await db.exists('ix:count')):
		print('REDIS NOT EMPTY; ABORTING!')
		await db.close()
		return
	async with db.pipeline() as pipe:
		pipe.hset('ix:count', 'id', 1)
		pipe.hset('ix:email', email_handler, 'id:0')
		pipe.hset('id:0', mapping={
			'id':0,
			'email':email_handler,
			'name':admin_name,
			'token':'RANDOM_INITIAL_TOKEN',
			'login_trys':0,
			'date_registered':iso_date(),
			'role':'ADMIN'})
		await pipe.execute()
	await db.close()
	print('REDIS INITIALIZED!')

asyncio.run(main())