#Update Admin Account
import asyncio
from redis import asyncio as aioredis
from env import admin_name, email_handler

async def main():
	db = await aioredis.from_url('unix:/var/run/redis/redis.sock', decode_responses=True)
	await db.delete('id:0')
	await db.hset('id:0', mapping={
		'id':0,
		'email':email_handler,
		'name':admin_name,
		'token':'RANDOM_INITIAL_TOKEN',
		'login_trys':0,
		'role':'ADMIN'})
	await db.close()
	print('ADMIN SET')

asyncio.run(main())