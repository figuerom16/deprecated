from fastapi import FastAPI, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from api import admin, token, user
from redis import asyncio as aioredis
import env

#Create FastAPI app
app = FastAPI()

app.add_middleware(
	CORSMiddleware,
	allow_origins=[env.front_server],
	allow_credentials=True,
	allow_methods=['*'],
	allow_headers=['*']
)

#Create DB connection
@app.on_event('startup')
async def startup_event():
	app.db = await aioredis.from_url('unix:/var/run/redis/redis.sock', decode_responses=True) #pyright:ignore

@app.on_event('shutdown')
async def shutdown_event():
	await app.db.close() #pyright:ignore

@app.exception_handler(RequestValidationError)
def validation_exception_handler(req:Request, exc:RequestValidationError):
	errors = []
	for e in exc.errors(): errors.append('ERROR '+str(e['loc'][1]).upper()+': '+e['msg'])
	return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content=jsonable_encoder({'detail':errors}))

#Router Routes
app.include_router(admin.router)
app.include_router(token.router)
app.include_router(user.router)

#Debug Server
if __name__ == '__main__':
	import uvicorn
	uvicorn.run(app, host='127.0.0.1', port=8000, log_level='debug')