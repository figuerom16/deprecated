from typing import Union
from pydantic import BaseModel, EmailStr, validator

class UserBase(BaseModel):
	email:EmailStr
	@validator('email')
	def to_lower(cls, v): return v.lower()

	class Config:
		min_anystr_length = 3
		max_anystr_length = 64
		anystr_strip_whitespace = True

class UserNew(UserBase): name:Union[str,None]

class UserAccount(UserBase):
	name:str
	bearer_update:Union[str,None]
	locked:Union[str,None]
	role:Union[str,None]

class AdminView(UserAccount):
	id:Union[int,None]
	login_trys:Union[int,None]
	date_login:Union[str,None]
	date_registered:Union[str,None]

class UserCookie(BaseModel):
	name:str
	value:str
	path:str