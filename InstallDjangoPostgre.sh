#!/bin/bash
echo "This is a template of how to fully press an async Django server in LXC Debian"
echo "Your project name will be the usernames and folder names (under /opt)... for everything!"
echo "You can always change both later"
echo "**********************************"
echo
echo "WHAT IS THE PROJECT NAME?: "
echo "(Forced to lower and spaces are removed)"
echo "(Use underscores as this will be used for usernames)"
read project
project=$(echo ${project,,} | tr -d " \t\n\r")
echo "Project Name: $project"
read -r -p "DJANGO PASSWORD: " django_password
read -r -p "DATABASE PASSWORD: " db_password
read -r -p "Number of Workers (2xcpu_cores)+1 [3]: " workers
workers=${workers:-3}
read -r -p "PostgreSQL database on a separate server? [y/N]: " db
db=${db,,}
if [[ "$db" =~ ^(yes|y)$ ]]
then
	echo
	echo "Make sure the database on the other server is already setup before continuing."
	echo "Example steps below for Database Setup"
	echo "apt install postgresql -y"
	echo "sed -i \"s|#listen_addresses =.*|listen_addresses = '*' # what IP address(es) to listen on;|\" /etc/postgresql/*/main/postgresql.conf"
	echo "echo host    all             all             0.0.0.0/0		md5 >> /etc/postgresql/*/main/pg_hba.conf"
	echo "systemctl restart postgresql"
	echo "su - postgres"
	echo "psql"
	echo "CREATE DATABASE $project;"
	echo "CREATE USER $project WITH PASSWORD '$db_password';"
	echo "ALTER ROLE $project SET client_encoding TO 'utf8';"
	echo "ALTER ROLE $project SET default_transaction_isolation TO 'read committed';"
	echo "ALTER ROLE $project SET timezone TO 'UTC';"
	echo "GRANT ALL PRIVILEGES ON DATABASE $project TO $project;"
	echo "\q"
	read -r -p "External database ip address: " db_ip
else
	runuser -l postgres -c "cat << EOT > temp.sql
	CREATE DATABASE $project;
	CREATE USER $project WITH PASSWORD '$db_password';
	ALTER ROLE $project SET client_encoding TO 'utf8';
	ALTER ROLE $project SET default_transaction_isolation TO 'read committed';
	ALTER ROLE $project SET timezone TO 'UTC';
	GRANT ALL PRIVILEGES ON DATABASE $project TO $project;
	\q
EOT"
	runuser -l postgres -c 'psql -f temp.sql'
	runuser -l postgres -c 'rm temp.sql'
fi
cd /opt
python3 -m venv venv
echo "alias venv='source /opt/venv/bin/activate'" >> ~/.bashrc
echo "source /opt/venv/bin/activate" >> ~/.bashrc
source /opt/venv/bin/activate
pip install -U pip setuptools wheel
pip install django gunicorn uvicorn[standard] psycopg2-binary
django-admin startproject $project && cd $project
cat << EOT > requirements.txt
django
gunicorn
uvicorn[standard]
psycopg2-binary
EOT
KEY=$(python manage.py shell -c 'import re;from django.core.management import utils; print(re.escape(utils.get_random_secret_key()))')
sed -i "s|SECRET_KEY =.*|SECRET_KEY = '$KEY'|" $project/settings.py
sed -i "s|ALLOWED_HOSTS =.*|ALLOWED_HOSTS = ['*']|" $project/settings.py
sed -i "/'ENGINE':.*/d" $project/settings.py
sed -i "/'NAME': BASE_DIR.*/d" $project/settings.py
sed -i "/'default': {/a\ \ \ \ \ \ \ \ 'ENGINE': 'django.db.backends.postgresql_psycopg2',\n        'NAME': '$project',\n        'USER': '$project',\n        'PASSWORD': '$db_password',\n        'HOST': '${db_ip:-localhost}',\n        'PORT': ''," $project/settings.py
sed -i "s|static/|/static/|" $project/settings.py
sed -i "/\/static\//a import os\nSTATIC_ROOT = os.path.join(BASE_DIR, 'static/')" $project/settings.py
python manage.py makemigrations
python manage.py migrate
DJANGO_SUPERUSER_PASSWORD=$django_password python manage.py createsuperuser --username $project --email admin@email.com --noinput
python manage.py collectstatic
deactivate
echo "alias fixperm='chown -R www-data:www-data /opt/*'" >> ~/.bashrc
chown -R www-data:www-data /opt/*
cat << EOT > /etc/systemd/system/gunicorn.socket
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target
EOT
cat << EOT > /etc/systemd/system/gunicorn.service
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=www-data
Group=www-data
WorkingDirectory=/opt/$project
ExecStart=/opt/venv/bin/gunicorn --access-logfile - -k uvicorn.workers.UvicornWorker --workers $workers --bind=unix:/run/gunicorn.sock $project.asgi:application

[Install]
WantedBy=multi-user.target
EOT
systemctl daemon-reload
systemctl start gunicorn.socket
systemctl enable gunicorn.socket
cat << EOT > /etc/nginx/sites-available/$project
server {
	listen 80;
	charset utf-8;
	root /opt/$project/;
	location = /favicon.ico {access_log off; log_not_found off;}
	location /static/ {access_log off;}
	location / {
		include proxy_params;
		proxy_pass http://unix:/run/gunicorn.sock;
	}
}
EOT
ln -s /etc/nginx/sites-available/$project /etc/nginx/sites-enabled/$project
rm /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
systemctl restart nginx
echo "cd /opt/$project" >> ~/.bashrc
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
echo
echo "Aliases created in bashrc. Reboot or run below to use."
echo "alias venv='source /opt/venv/bin/activate'"
echo "alias fixperm='chown -R www-data:www-data /opt/*'"
