import readline from 'node:readline'
import child_process from 'node:child_process'
import fs from 'node:fs'
import { parseArgs } from 'node:util'

function input(prompt){
  const rl = readline.createInterface({input:process.stdin, output:process.stdout})
    return new Promise( resolve => 
    {rl.question(prompt, answer => {
      rl.close()
      resolve(answer)
})})}

//Choices
const frameworks = ['Vite (Svelte)', 'SvelteKit', 'Astro']
let frameworks_short = []
for (const key in frameworks) frameworks_short.push(frameworks[key][0].toLowerCase()) 
const themes = ['custom', 'Skeleton', 'Rocket', 'Vintage', 'Modern', 'Sahara', 'Seafoam', 'Hamlindigo', 'Gold Nouveau', 'Crimson']

//Defaults
let project = 'skeleton_auto'
let framework_number = 1 //SvelteKit
let theme_number = 1 //skeleton
let tailwind_extras = '@tailwindcss/forms+@tailwindcss/typography'
let auto = false

//Argument Parsing with parseArgs
const options = {
  project: {type:'string', short:'p'},
  framework: {type:'string', short:'f'},
  theme: {type:'string', short:'t'},
  extras: {type:'string', short:'e'},
  auto: {type:'boolean', short:'a'},
  help: {type:'boolean', short:'h'}
}
const { values } = parseArgs({ options })
if (process.argv.length > 2) {
  let errors = ''

  if (values.help) {
    console.log(`Install script options
--project -p <project_name> The folder and project name
--framework -f <Vite, Sveltekit, or Astro> This only looks at the first letter so -f v for Vite will work also.
--theme -t <custom, skeleton, rocket, vintage, modern, sahara, seasfoam, hamlindigo, gold nouveau, crimson>
--extras -e <@tailwindcss/forms+@tailwindcss/typography> this does an npm i -D on any package specified there and adds it to the plugins in tailwind.config.cjs.
--auto -a boolean type. Including this will cause the program to do an automated install.

If arguments aren't specified it will use defaults:
project: ${project}
framework: ${frameworks[framework_number]}
theme: ${themes[theme_number]}
tailwind_extras = ${tailwind_extras}`)
    process.exit()
  }

  if (values.project) project = values.project

  if (values.framework) framework_number = frameworks_short.indexOf(values.framework[0].toLowerCase())
  else if (framework_number == -1) errors += 'Error: Framework Does Not Exist\n'

  if (values.theme) theme_number = themes.indexOf(values.theme.toLowerCase())
  else if (theme_number == -1) errors += 'Error: Theme Does Not Exist\n'

  if (values.extras) tailwind_extras = values.extras
  if (values.auto) auto = values.auto

  if (errors) {
    console.log(errors)
    process.exit()
  }
}

if (fs.existsSync(project)) {
  console.log("ERROR: Project Directory already exists. ABORTING")
  process.exit()
}

//MAIN PROGRAM
async function main() {

//Manual User Questions
if (process.argv.length < 3) {
  let answer = ''
  framework_number = -1
  theme_number = -1

  //Project Name and Framework
  answer = await input('WHAT IS THE PROJECT NAME?: ')
  project = answer.replace(' ','_')
  console.log('\nWHICH FRAMEWORK?: ')
  for (const key in frameworks) console.log(key+') '+frameworks[key])
  while (framework_number < 0 || framework_number > frameworks.length - 1) {
    framework_number = await input('SELECT A FRAMEWORK BY #: ')
  }
  
  //Choose Theme
  console.log('\nhttps://skeleton.brainandbonesllc.com/guides/themes')
  for (const key in themes) console.log(key+') '+themes[key])
  while (theme_number < 0 || theme_number > themes.length - 1) {
    theme_number = await input('SELECT A THEME BY #: ')
  }
  
  //Tailwind Extras
  tailwind_extras = ''
  answer = await input('\nADD TAILWIND FORMS? [y/N]?: ')
  if (answer) answer = answer[0].toLowerCase()
  if (answer == 'y') tailwind_extras += '@tailwindcss/forms+'
  answer = await input('ADD TAILWIND TYPOGRAPHY? [y/N]?: ')
  if (answer) answer = answer[0].toLowerCase()
  if (answer == 'y') tailwind_extras += '@tailwindcss/typography'
}

//Frameworks: 0 Vite (Svelte), 1 SvelteKit, 2 Astro 
//0: Install Framework
//1: Install Tailwind
//2: Custom Theme Import
let configs = [
  [`npm create vite@latest ${project} -- --template svelte-ts`,'npx svelte-add@latest tailwindcss','./theme.css'],
  [`npm create svelte@latest ${project}`,'npx svelte-add@latest tailwindcss','../theme.postcss'],
  [`npm create astro@latest ${project}`,'npx astro add svelte tailwind','../styles/theme.css']
]

let theme = ''
if (theme_number == 0) theme = configs[framework_number][2]
else theme = `@skeletonlabs/skeleton/themes/theme-${themes[theme_number].toLowerCase()}.css`

if (!auto) {
  //npm create
  child_process.execSync(configs[framework_number][0],{stdio:[0,1,2]})
  //switch to project working directory
  process.chdir(project)
  //npx additions
  child_process.execSync(configs[framework_number][1],{stdio:[0,1,2]})
}
//One line installers
else if (auto && framework_number == 0){
  child_process.execSync(`npm init @svelte-add/vite@latest ${project} -y -- --with typescript+tailwindcss`,{stdio:[0,1,2]})
  process.chdir(project)
}
else if (auto && framework_number == 1){
  child_process.execSync(`npm init @svelte-add/kit@latest ${project} -y -- --with typescript+tailwindcss`,{stdio:[0,1,2]})
  process.chdir(project)
}
else if (auto && framework_number == 2){
  child_process.execSync(`npm init @svelte-add/vite@latest ${project} -y -- --with typescript+tailwindcss`,{stdio:[0,1,2]})
  child_process.execSync(`npx astro add svelte -y`,{stdio:[0,1,2]})
  process.chdir(project)
}

//Install Tailwind Extras and Skeleton
child_process.execSync('npm i -D @skeletonlabs/skeleton '+tailwind_extras.replace('+',' '),{stdio:[0,1,2]})
if (tailwind_extras) tailwind_extras = '    require("'+tailwind_extras.replace('+','"),\n    require("')+'")'

//Tailwind Config Customization
fs.readFile('tailwind.config.cjs', 'utf8', function(err,data){
  if (err) throw(err)
  else {
    data = data.replace(' = {', ` = {\n  darkMode: "class",`)
    let content = data.match(/.*content: .*/)
    data = data.replace(content[0],content[0].slice(0,-2) + `,\n    require('path').join(require.resolve('@skeletonlabs/skeleton'), '../**/*.{html,js,svelte,ts}')],`)
    data = data.replace('plugins: []',
  `plugins: [
    require("@skeletonlabs/skeleton/tailwind/theme.cjs"),\n`+tailwind_extras+`\n  ]`)
    fs.writeFile('tailwind.config.cjs', data, function (err){})
  }
})

//Vite (Svelte) Customization
if (framework_number == 0) {
  fs.readFile('src/main.ts', 'utf8', function(err,data){
    if (err) throw(err)
    else {
      data =
`import "${theme}";
import "@skeletonlabs/skeleton/styles/all.css";\n` + data
      fs.writeFile('src/main.ts', data, function (err){})
    }
  })
}

//SvelteKit Customization
else if (framework_number == 1) {
  fs.readFile('src/routes/+layout.svelte', 'utf8', function(err,data){
    if (err) throw(err)
    else {
    data = data.replace(/<script.*/,
`<script lang="ts">
  import "${theme}";
  import "@skeletonlabs/skeleton/styles/all.css";`)
      fs.writeFile('src/routes/+layout.svelte', data, function (err){})
    }
  })
}

//Astro Customization
else if (framework_number == 2) {
  const astro =
`// Theme
import '${theme}';
import '@skeletonlabs/skeleton/styles/all.css';
import '../styles/base.css';
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta name="generator" content={Astro.generator} />
    <title>Astro</title>
</head>
<body>
    <slot />
</body>
</html>`
  fs.writeFile('src/layouts/LayoutBasic.astro', astro, function (err){})
}

//Tailwind Customization (Astro app.postcss doesn't exist)
if (framework_number != 2) {
  fs.readFile('src/app.postcss', 'utf8', function(err,data) {
    if (err) throw(err)
    else {
    data = data.replace(/@tailwind.*\n/g,'')
    fs.writeFile('src/app.postcss', data, function (err){})
    }
  })
}

//If Custom Theme
if (theme_number == 0) {
  console.log('\nCreate your custom theme here: https://www.skeleton.dev/guides/themes')
  console.log('YOUR PROJECT WILL NOT RUN WITHOUT YOUR CUSTOM THEME!')
}

console.log('\ncd ' + project)
console.log('npm run dev')
console.log('Your project now has bones!')
}

main()