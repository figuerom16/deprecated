#!/bin/bash
#This script looks for errors from websites.
#Returns their status as body text for HTML5.
output=/opt/static/static/website_health.html
>$output
online='<button class="button is-small is-rounded is-success">ONLINE</button>'
offline='<button class="button is-small is-rounded is-danger">OFFLINE</button>'
query() {
	curl -sSf $1 2>response.log 1>/dev/null
	if [ -s response.log ]; then
		echo "$offline <b>$1</b>" $(cat response.log) >> $output
	else
		echo "$online <b>$1</b>" >> $output
	fi
}
query google.com
query mattascale.com
query test.mattascale.com
query ocinpaoseubf.com
sed -i "s|$|<br>|g" $output

#This script runs top and parses the output.
#Returns top information as body text for HTML5.
for i in {1..6}; do
top -bn1 | head -5 | sed "s|$|<br>|g" > top.txt
cat top.txt > /opt/static/static/vm_stats.html
cpu_load=$(cat top.txt | grep %Cpu | grep -Eo '......id' | cut -c 1-5 | tr -d " ")
cpu_load=$(bc <<< "100 - $cpu_load")
memory_total=$(cat top.txt | grep "MiB Mem" | tr -s " " | cut -d" " -f4)
memory=$(cat top.txt | grep "MiB Mem" | tr -s " " | cut -d" " -f8)
load_avg=$(cat top.txt | grep -Eo 'load average:.*' | tr -d ",")
load_avg=${load_avg:14:-4}
load_1=$(echo $load_avg | cut -d" " -f1)
load_5=$(echo $load_avg | cut -d" " -f2)
load_15=$(echo $load_avg | cut -d" " -f3)
cat << EOT >> /opt/static/static/vm_stats.html
<br>
<table class="table">
<tbody>
<tr><td width="20%">CPU LOAD%</td><td width="80%"><progress class="progress is-danger" value="$cpu_load" max="100">$cpu_load</progress></td></tr>
<tr><td width="20%">MEM ${memory_total}MB</td><td width="80%"><progress class="progress is-link" value="$memory" max="$memory_total">$memory</progress></td></tr>
<tr><td width="20%">LOAD 1Min</td><td width="80%"><progress class="progress is-success" value="$load_1" max="1">$load_1</progress></td></tr>
<tr><td width="20%">LOAD 5Min</td><td width="80%"><progress class="progress is-success" value="$load_5" max="1">$load_5</progress></td></tr>
<tr><td width="20%">LOAD 15Min</td><td width="80%"><progress class="progress is-success" value="$load_15" max="1">$load_15</progress></td></tr>
</tbody>
</table>
EOT
sleep 10
done

#Build Simple HTML5 dashboard with Bash
#Build col function
build_col() {
	echo '<div class="column buttons is-one-fifth">' >> index.html
	for item in "$@"; do
			IFS='|' read -ra link <<< $item
			cat << EOT >> index.html
			<a class="button title is-medium is-justify-content-flex-start is-fullwidth mb-0 ${link[2]}" href="${link[0]}" $new_tab>
			<figure class="image"><img src="/img/${link[3]}">
			</figure>&nbsp${link[1]}</a>
		<div class="box p-0">${link[4]}</div>
EOT
		done
	echo '</div>' >> index.html
}
#Get variables
. bashdash.conf
#Build index.html
cat << EOT > index.html
<!DOCTYPE html>
<html lang="en" style="background-image: $bg_input; background-size: cover; background-size: 100% 100%; background-attachment: fixed;">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/bulma.min.css">
	<title>$title</title>
</head>
<body>
<section class="hero is-fullheight">
	<div class="hero-body">
	<div class="container is-fluid has-text-centered">
	<div class="columns is-centered">
EOT
build_col "${col0[@]}"
build_col "${col1[@]}"
build_col "${col2[@]}"
build_col "${col3[@]}"
build_col "${col4[@]}"
cat << EOT >> index.html
	</div>
	</div>
	</div>
</section>
</body>
</html>
EOT