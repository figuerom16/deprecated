#!/bin/bash
echo "WHAT IS THE PROJECT NAME?: "
echo "(Forced to lower and spaces are removed)"
echo "(Use underscores)"
read project
project=$(echo ${project,,} | tr -d " \t\n\r")
echo "Project Name: $project"
cd /opt
mkdir $project && cd $project
touch index.html
echo "alias fixperm='chown -R www-data:www-data /opt/*'" >> ~/.bashrc
chown -R www-data:www-data /opt/*
cat << EOT > /etc/nginx/sites-available/$project
server {
	listen 80;
	#ssi on;
	charset utf-8;
	root /opt/$project/;
}
EOT
ln -s /etc/nginx/sites-available/$project /etc/nginx/sites-enabled/$project
rm /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
systemctl restart nginx
echo "cd /opt/$project" >> ~/.bashrc
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
echo
echo "Alias created in bashrc. Reboot or run below to use."
echo "alias fixperm='chown -R www-data:www-data /opt/*'"
